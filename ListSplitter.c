/*
** ListSplitter.c for List in /home/lerest_m/Programmation/C/ChainedList/
**
** Made by Mewen Le Reste
** Login   <mewen.lereste@epitech.eu>
**
** Started on  Wed Apr 19 14:44:30 2017 Mewen Le Reste
** Last update Mon May 29 16:38:16 2017 Mewen Le Reste
*/

#include "List.h"

static int	list_compare_value(list_type first, list_type second)
{
  if (first == second)
    return (0);
  return (1);
}

static int	split_from(List **dest, List **src, t_split *struc)
{
  t_node	*node;
  List		*list;

  if (!(list = list_init()))
    return (1);
  while ((node = list_get_node(src, struc->count)))
    {
      if (struc->fct(node->value, struc->regex) == 0)
	{
	  if (!list_init(list))
	    list_add_back(dest, list);
	  else
	    list_delete(&list);
	  return (0);
	}
      list_add_back(&list, node->value);
      ++struc->count;
    }
  list_add_back(dest, list);
  return (1);
}

List		*listSplit(List **list, list_type regex, listCmpFct fct)
{
  t_split	struc;
  List		*splitList;
  int		ret;

  struc.count = 0;
  struc.regex = regex;
  if (!(splitList = list_init()))
    return (NULL);
  if (!fct)
    fct = &list_compare_value;
  struc.fct = fct;
  while ((ret = split_from(&splitList, list, &struc)) == 0)
    ++struc.count;
  return (splitList);
}
