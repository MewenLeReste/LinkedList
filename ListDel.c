/*
** ListDel.c for List in /home/lerest_m/Programmation/Piscine/Tek 2/cpp_d02a/ex00/
**
** Made by Mewen Le Reste
** Login   <mewen.lereste@epitech.eu>
**
** Started on  Tue Apr 18 10:00:59 2017 Mewen Le Reste
** Last update Mon May 29 16:57:55 2017 Mewen Le Reste
*/

#include "List.h"

static bool	list_del(List **list, t_node *entry)
{
  t_node	*tmp;
  t_node	**indirect;

  if (!entry)
    return (false);
  indirect = &(*list)->head;
  while ((*indirect) != entry)
    indirect = &(*indirect)->next;
  tmp = *indirect;
  if (entry->next)
    entry->next->prev = entry->prev;
  *indirect = entry->next;
  --(*list)->size;
  if (entry->next)
    reset_head_tail_from(list, entry->next);
  else
    reset_head_tail_from(list, entry->prev);
  free(tmp);
  return (true);
}

bool		list_del_front(List **list)
{
  bool		ret;

  ret = list_del(list, (*list)->head);
  return (ret);
}

bool		list_del_back(List **list)
{
  bool		ret;

  ret = list_del(list, (*list)->tail);
  return (ret);
}

bool		list_del_position(List **list, unsigned int position)
{
  bool		ret;

  ret = list_del(list, list_get_node(list, position));
  return (ret);
}
