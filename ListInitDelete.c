/*
** ListInitDelete.c for List in /home/lerest_m/Programmation/C/ChainedList/
**
** Made by Mewen Le Reste
** Login   <mewen.lereste@epitech.eu>
**
** Started on  Tue Apr 18 16:30:16 2017 Mewen Le Reste
** Last update Mon May 29 16:28:32 2017 Mewen Le Reste
*/

#include "List.h"

static void	list_delete_node(t_node *node)
{
  if (node->next)
    list_delete_node(node->next);
  free(node);
}

void		list_delete(List **list)
{
  if ((*list)->head)
    list_delete_node((*list)->head);
  (*list)->init = false;
  (*list)->size = 0;
  (*list)->head = NULL;
  (*list)->tail = NULL;
  free(*list);
}

List		*list_init()
{
  List		*list;

  if (!(list = malloc(sizeof(List))))
    return (false);
  list->init = false;
  list->size = 0;
  list->head = NULL;
  list->tail = NULL;
  return (list);
}
