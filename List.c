/*
** List.c for List in /home/lerest_m/Programmation/C/ChainedList/
**
** Made by Mewen Le Reste
** Login   <mewen.lereste@epitech.eu>
**
** Started on  Tue Apr 18 15:34:13 2017 Mewen Le Reste
** Last update Mon May 29 16:17:42 2017 Mewen Le Reste
*/

#include "List.h"

unsigned int	list_size(List *list)
{
  return (list->size);
}

bool		list_is_empty(List *list)
{
  return (!list->init);
}

void		list_dump(List *list, listDumpFct fct)
{
  t_node	*node;

  if (!fct || !list->init)
    return ;
  node = list->head;
  while (node->next)
    {
      fct(node->value);
      node = node->next;
    }
  fct(node->value);
}

void		list_for_each(List **list, listForEachFct fct, void *struc)
{
  t_node	*node;

  if (!fct || !(*list)->init)
    return ;
  node = (*list)->head;
  while (node)
    {
      if (!fct(node->value, struc))
	return ;
      node = node->next;
    }
}

void		reset_head_tail_from(List **list, t_node *node)
{
  t_node	*tmp;

  tmp = node;
  if (!node)
    {
      (*list)->head = NULL;
      (*list)->tail = NULL;
      (*list)->init = false;
      return ;
    }
  while (node->prev)
    node = node->prev;
  (*list)->head = node;
  while (tmp->next)
    tmp = tmp->next;
  (*list)->tail = tmp;
}
