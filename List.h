/*
** List.h for List in /home/lerest_m/Programmation/C/List/
**
** Made by Mewen Le Reste
** Login   <mewen.lereste@epitech.eu>
**
** Started on  Tue Apr 18 12:15:10 2017 Mewen Le Reste
** Last update Mon May 29 16:51:36 2017 Mewen Le Reste
*/

#ifndef LIST_H_
# define LIST_H_

/* Include for malloc and free */
# include <stdlib.h>
/* Include for the use of boolean */
# include <stdbool.h>

# ifndef LIST_TYPE_
#  define LIST_TYPE_

typedef void		*list_type; /* template T */

# endif /* !LIST_TYPE_ */

/* Typedef of all the used function pointer*/
typedef int		(*listCmpFct)(list_type, list_type);
typedef void		(*listDumpFct)(list_type);
typedef bool		(*listForEachFct)(list_type, void *);
typedef list_type	(*listCpyFct)(list_type);

/* Enum that can replace bool if not allowed */
/*typedef enum	e_bool
  {
    False,
    True
  }		t_bool;*/

/* Structure use for the norm of the splitting function :D */
typedef struct		s_split
{
  list_type		regex;
  int			count;
  listCmpFct		fct;
}			t_split;

/* Node composing the list */
typedef struct		s_node
{
  list_type		value;
  struct s_node		*next;
  struct s_node		*prev;
}			t_node;

/* Main structure containing the list and information about it */
typedef struct		s_list
{
  t_node		*head;
  t_node		*tail;
  unsigned int		size;
  bool			init;
}			List;

/* Initialisation and deletion of the list */
List			*list_init(); /* Constructor */
void			list_delete(List **); /* Destructor */

/* Information getter */
unsigned int		list_size(List *);
bool			list_is_empty(List *);
void			list_dump(List *, listDumpFct); /* operator << */

/* List parser */
bool			list_sort(List **, listCmpFct);
void			list_for_each(List **, listForEachFct, void *);
void			reset_head_tail_from(List **, t_node *);

/* Adding functiong */
bool			list_add_front(List **, list_type);
bool			list_add_back(List **, list_type);
bool			list_add_position(List **, list_type, unsigned int);

/* Deleting function */
bool			list_del_front(List **);
bool			list_del_back(List **);
bool			list_del_position(List **, unsigned int);

/* Node getter */
t_node			*list_get_first_of(List **, list_type, listCmpFct);
t_node			*list_get_last_of(List **, list_type, listCmpFct);
t_node			*list_get_min(List **, listCmpFct);
t_node			*list_get_max(List **, listCmpFct);
t_node			*list_get_node(List **, unsigned int);

/* Value getter */
list_type		list_get_front(List **);
list_type		list_get_back(List **);
list_type		list_get_position(List **, unsigned int); /* operator [] */

/* List operator */
bool			list_concat(List **, List **, listCpyFct);/* operator + */
List			*list_split(List **, list_type, listCmpFct);/* operator / */
List			*list_cpy(List **, listCpyFct); /* operator = */

#endif /* !LIST_H_ */
