/*
** ListGetOccurenceOf.c for List in /home/lerest_m/Programmation/C/ChainedList/
**
** Made by Mewen Le Reste
** Login   <mewen.lereste@epitech.eu>
**
** Started on  Tue Apr 18 15:53:07 2017 Mewen Le Reste
** Last update Mon May 29 16:40:19 2017 Mewen Le Reste
*/

#include "List.h"

static t_node	*list_min_max(List **list, listCmpFct fct, int cmp)
{
  t_node	*stock;
  t_node	*node;

  if (!fct || !list)
    return (NULL);
  node = (*list)->head;
  stock = node;
  while (node)
    {
      if (fct(node->value, stock->value) == cmp)
	stock = node;
      node = node->next;
    }
  return (stock);
}

t_node		*list_get_min(List **list, listCmpFct fct)
{
  t_node		*ret;

  ret = list_min_max(list, fct, -1);
  return (ret);
}

t_node		*list_get_max(List **list, listCmpFct fct)
{
  t_node		*ret;

  ret = list_min_max(list, fct, 1);
  return (ret);
}

t_node		*list_get_first_of(List **list, list_type value, listCmpFct fct)
{
  t_node	*node;

  if (!list)
    return (NULL);
  node = (*list)->head;
  while (node)
    {
      if (fct(node->value, value) == 0)
	return (node);
      node = node->next;
    }
  return (NULL);
}

t_node		*list_get_last_of(List **list, list_type value, listCmpFct fct)
{
  t_node	*node;

  if (!list)
    return (NULL);
  node = (*list)->tail;
  while (node)
    {
      if (fct(node->value, value) == 0)
	return (node);
      node = node->prev;
    }
  return (NULL);
}
