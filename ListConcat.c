/*
** ListConcat.c for List in /home/arnaul_c/astek/ChainedList
**
** Made by Céline Arnault
** Login   <arnaul_c@epitech.net>
**
** Started on  Tue Apr 18 15:51:18 2017 Céline Arnault
** Last update Mon May 29 16:24:28 2017 Mewen Le Reste
*/

#include "List.h"

bool		list_concat(List **dest, List **src, listCpyFct fct)
{
  t_node	*node;

  node = (*src)->head;
  if (!node)
    return (false);
  while (node)
    {
      list_add_back(dest, fct(node->value));
      node = node->next;
    }
  return (true);
}

List		*list_cpy(List **src, listCpyFct fct)
{
  List		*list;

  if (!(list = list_init()))
    return (NULL);
  list_concat(&list, src, fct);
  return (list);
}
