##
## Makefile for List in /home/lerest_m/Programmation/C/ChainedList/
##
## Made by Mewen Le Reste
## Login   <mewen.lereste@epitech.eu>
##
## Started on  Tue Apr 18 15:11:34 2017 Mewen Le Reste
## Last update Fri Apr 21 15:03:29 2017 Mewen Le Reste
##

SRC	= List.c \
			ListAdd.c \
			ListConcat.c \
			ListDel.c \
			ListGet.c \
			ListGetOccurenceOf.c \
			ListInitDelete.c \
			ListSorter.c \
			ListSplitter.c

TESTSRC = Test/main.c \
					Test/test.c \
					Test/test1.c \
					Test/test2.c \
					Test/test3.c \
					Test/eat.c \
					Test/test4.c

OBJ	= $(SRC:.c=.o)

TESTOBJ = $(TESTSRC:.c=.o)

NAME	= List.a

TESTNAME = test

CFLAGS	= -W -Wall -Werror -pedantic -I..

all: $(NAME)

$(NAME): $(OBJ)
	ar rc $(NAME) $(OBJ)

$(TESTNAME): $(TESTOBJ)
	gcc -o $(TESTNAME) $(TESTOBJ) $(NAME)

clean:
	rm -f $(OBJ)
	rm -f $(TESTOBJ)

fclean: clean
	rm -f $(NAME)
	rm -f $(TESTNAME)

re: fclean all

.PHONY: all clean fclean re
