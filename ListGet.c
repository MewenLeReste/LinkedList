/*
** ListGet.c for List in /home/lerest_m/Programmation/Piscine/Tek 2/cpp_d02a/ex00/
**
** Made by Mewen Le Reste
** Login   <mewen.lereste@epitech.eu>
**
** Started on  Tue Apr 18 10:04:43 2017 Mewen Le Reste
** Last update Mon May 29 16:51:21 2017 Mewen Le Reste
*/

#include "List.h"

t_node		*list_get_node(List **list, unsigned int position)
{
  t_node	*node;

  if (!list || position >= list_size(*list))
    return (NULL);
  node = (*list)->head;
  while (position)
    {
      node = node->next;
      --position;
    }
  return (node);
}

list_type	list_get_front(List **list)
{
  t_node	*node;

  if (!(node = list_get_node(list, 0)))
    return (NULL);
  return (node->value);
}

list_type	list_get_back(List **list)
{
  t_node	*node;

  if (!(node = list_get_node(list, (*list)->size - 1)))
    return (NULL);
  return (node->value);
}

list_type	list_get_position(List **list, unsigned int position)
{
  t_node	*node;

  if (!(node = list_get_node(list, position)))
    return (NULL);
  return (node->value);
}
