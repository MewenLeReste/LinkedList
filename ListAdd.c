/*
** ListAdd.c for List in /home/lerest_m/Programmation/Piscine/Tek 2/cpp_d02a/ex00/
**
** Made by Mewen Le Reste
** Login   <mewen.lereste@epitech.eu>
**
** Started on  Tue Apr 18 09:57:38 2017 Mewen Le Reste
** Last update Mon May 29 16:39:42 2017 Mewen Le Reste
*/

#include "List.h"

static bool	list_add_node(List **list, list_type elem,
			      t_node *next, t_node *prev)
{
  t_node	*node;

  if (!(node = malloc(sizeof(t_node))))
    return (false);
  node->value = elem;
  node->next = next;
  if (next)
    next->prev = node;
  node->prev = prev;
  if (prev)
    prev->next = node;
  reset_head_tail_from(list, node);
  ++(*list)->size;
  (*list)->init = true;
  return (true);
}

bool		list_add_front(List **list, list_type elem)
{
  bool		ret;

  ret = list_add_node(list, elem, (*list)->head, NULL);
  return (ret);
}

bool		list_add_back(List **list, list_type elem)
{
  bool		ret;

  ret = list_add_node(list, elem, NULL, (*list)->tail);
  return (ret);
}

bool		list_add_position(List **list, list_type elem, unsigned int position)
{
  bool		ret;

  if (position > (*list)->size)
    return (false);
  ret = list_add_node(list, elem, list_get_node(list, position),
		      list_get_node(list, (position - 1)));
  return (ret);
}
