/*
** ListSorter.c for List in /home/lerest_m/Programmation/C/ChainedList/
**
** Made by Mewen Le Reste
** Login   <mewen.lereste@epitech.eu>
**
** Started on  Fri Apr 21 14:43:18 2017 Mewen Le Reste
** Last update Mon May 29 16:31:46 2017 Mewen Le Reste
*/

#include "List.h"

bool		list_sort(List **list, listCmpFct fct)
{
  t_node	*node1;
  t_node	*node2;
  list_type	tmp;

  if (!fct)
    return (false);
  node1 = (*list)->head;
  while (node1)
    {
      node2 = node1->next;
      while (node2)
	{
	  if (fct(node1->value, node2->value) == -1)
	    {
	      tmp = node1->value;
	      node1->value = node2->value;
	      node2->value = tmp;
	    }
	  node2 = node2->next;
	}
      node1 = node1->next;
    }
  return (true);
}
